Source: jpeg-xl
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <malat@debian.org>
Section: graphics
Priority: optional
Standards-Version: 4.6.2
Build-Depends: asciidoc-base,
               cmake (>= 3.10),
               debhelper (>= 11),
               default-jdk [@JAVA_ARCHS@],
               docbook-xml,
               help2man,
               libavif-dev (>= 0.10.1),
               libbrotli-dev,
               libgdk-pixbuf-2.0-dev,
               libgflags-dev,
               libgif-dev (>= 5.1),
               libgmock-dev <!nocheck>,
               libgoogle-perftools-dev [@TCMALLOC_ARCHS@],
               libgtest-dev <!nocheck>,
               libhwy-dev (>= 1.0.2),
               libjpeg-dev,
               libjxl-testdata (>= 0.0~git20230428.07d667a) <!nocheck>,
               liblcms-dev | liblcms2-dev (>= 2.13),
               libopenexr-dev,
               libpng-dev,
               libwebp-dev,
               ninja-build,
               pkg-config
Build-Depends-Indep: doxygen, graphviz
Homepage: https://github.com/libjxl/libjxl
Vcs-Git: https://salsa.debian.org/debian-phototools-team/libjxl.git
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/libjxl
Rules-Requires-Root: no

Package: libjxl-tools
Architecture: any
Section: utils
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: libjpeg-turbo-progs
Suggests: netpbm
Description: JPEG XL Image Coding System - "JXL" (command line utility)
 The JPEG XL Image Coding System (ISO/IEC 18181) is a lossy and
 lossless image compression format. It has a rich feature set and is
 particularly optimized for responsive web environments, so that
 content renders well on a wide range of devices. Moreover, it includes
 several features that help transition from the legacy JPEG format.
 .
 This package installs the command line utilities.

Package: libjxl-dev
Architecture: any
Section: libdevel
Depends: libbrotli-dev,
         libhwy-dev,
         libjxl0.9 (= ${binary:Version}),
         liblcms-dev,
         ${misc:Depends}
Multi-Arch: same
Description: JPEG XL Image Coding System - "JXL" (development files)
 The JPEG XL Image Coding System (ISO/IEC 18181) is a lossy and
 lossless image compression format. It has a rich feature set and is
 particularly optimized for responsive web environments, so that
 content renders well on a wide range of devices. Moreover, it includes
 several features that help transition from the legacy JPEG format.
 .
 This package installs development files.

Package: libjxl0.9
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: JPEG XL Image Coding System - "JXL" (shared libraries)
 The JPEG XL Image Coding System (ISO/IEC 18181) is a lossy and
 lossless image compression format. It has a rich feature set and is
 particularly optimized for responsive web environments, so that
 content renders well on a wide range of devices. Moreover, it includes
 several features that help transition from the legacy JPEG format.
 .
 This package installs shared libraries.

Package: jpeg-xl-doc
Architecture: all
Section: doc
Depends: doc-base, ${misc:Depends}
Description: JPEG XL Image Coding System - "JXL" (documentation)
 The JPEG XL Image Coding System (ISO/IEC 18181) is a lossy and
 lossless image compression format. It has a rich feature set and is
 particularly optimized for responsive web environments, so that
 content renders well on a wide range of devices. Moreover, it includes
 several features that help transition from the legacy JPEG format.
 .
 This package installs the doxygen documentation.

Package: libjxl-devtools
Architecture: any
Section: utils
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: libavif-bin
Description: JPEG XL Image Coding System - "JXL" (dev command line utility)
 The JPEG XL Image Coding System (ISO/IEC 18181) is a lossy and
 lossless image compression format. It has a rich feature set and is
 particularly optimized for responsive web environments, so that
 content renders well on a wide range of devices. Moreover, it includes
 several features that help transition from the legacy JPEG format.
 .
 This package installs the devtools command line utilities.

Package: libjxl-gdk-pixbuf
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: JPEG XL Plugin for gdk-pixbuf
 This package installs the required files for reading JPEG XL files in
 GTK applications.

Package: libjpegxl-java
Architecture: @JAVA_ARCHS@
Section: java
Depends: ${java:Depends}, ${misc:Depends}, ${shlibs:Depends}
Suggests: java-virtual-machine
Description: JPEG XL Image Coding System - "JXL" (java bindings)
 The JPEG XL Image Coding System (ISO/IEC 18181) is a lossy and
 lossless image compression format. It has a rich feature set and is
 particularly optimized for responsive web environments, so that
 content renders well on a wide range of devices. Moreover, it includes
 several features that help transition from the legacy JPEG format.
 .
 This package installs the Java Bindings.

Package: libjpegli62
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Conflicts: libjpeg62, libjpeg62-turbo
Replaces: libjpeg62, libjpeg62-turbo
Provides: libjpeg62-turbo (= 1:1.3.1)
Description: Improved JPEG encoder and decoder implementation (shared libraries)
 JPEGLIS provides a JPEG encoder and decoder implementation that is
 API and ABI compatible with libjpeg62.
 .
 This package installs jpegli shared libraries.

Package: libjpegli62-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libc-dev, libjpegli62 (= ${binary:Version}), ${misc:Depends}
Conflicts: libjpeg62-dev,
           libjpeg62-turbo-dev,
           libjpeg7-dev,
           libjpeg8-dev,
           libjpeg9-dev
Replaces: libjpeg62-dev,
          libjpeg62-turbo-dev,
          libjpeg7-dev,
          libjpeg8-dev,
          libjpeg9-dev
Provides: libjpeg-dev
Description: Improved JPEG encoder and decoder implementation (development files)
 JPEGLIS provides a JPEG encoder and decoder implementation that is
 API and ABI compatible with libjpeg62.
 .
 This package installs jpegli jpeg62 headers.

Package: libjpegli-tools
Architecture: any
Section: utils
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Improved JPEG encoder and decoder implementation (command line utility)
 JPEGLIS provides a JPEG encoder and decoder implementation that is
 API and ABI compatible with libjpeg62.
 .
 This package installs the JPEGLI tools command line utilities.
